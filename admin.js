const adminBroReq =  require('admin-bro');
const adminBroExpReq = require('@admin-bro/express');
const express = require("express");
const adminBroMongoose = require ('@admin-bro/mongoose');
const mongoose = require('mongoose');

adminBroReq.registerAdapter(adminBroMongoose);

// ============
// Database


const filmeSchema = new mongoose.Schema({
    nome: String,
    data: Date,
    autor: String,
});

const filmesModel = mongoose.model("Filmes", filmeSchema);

const usuarioSchema = new mongoose.Schema({
    nome: String,
    sobrenome: String,
    dataCadastro: {type: Date, default: Date.now},
});

const usuariosModel = mongoose.model("Usuarios", usuarioSchema);

const adminBro = new adminBroReq({
    resources: [
        {
            resource: filmesModel
        },
        {
            resource: usuariosModel
        }
        ],
    rootPath: '/admin',
})

const router = adminBroExpReq.buildRouter(adminBro);

// ===============
//Server



const server = express();

server.use(adminBro.options.rootPath, router);
// ===============
// Run App

const run = async() => {
    await mongoose.connect("mongodb://localhost/crud"); 
    await server.listen(3300, () => console.log("Server started"));

    
}

run()



