# crud-admin-bro
Exercício 4 - Criação de um CRUD (Filmes).

Instalações necessárias:
- [NodeJs](https://nodejs.org/en/)
- [MongoDB](https://www.mongodb.com/try/download/community)
- [AdminBro.js](https://adminjs.co/)
- [Express](https://expressjs.com/)

# Passos:

1. Instalação do NodeJS para uso do npm como gestor de dependencias;
2. Instalação das dependencias do projeto (admin-bro);
- Decidi utilizar o Framework Admin-bro devido a sua facilidade na criação e integração com um banco de dados.
- O admin-bro só suporta banco de dados NQL, por isso utilizamos o MongoDB.

4. Após a instalação e requerimento das dependências, dei início a criação do esquema de dados em formato de objeto devido ao banco de dados não ser SQL (o esquema de "usuários" está apenas para teste).
5. Pela parte do servidor, foi criada uma função similar ao callback com o objetivo de só executar após o mongoose se conectar ao localhost/crus.

# Inicialização

localhost:3300/admin/Filmes
- [Ou clique aqui](http://localhost:3300/admin/resources/Filmes)





